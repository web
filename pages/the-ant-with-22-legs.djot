The Ant With 22 Legs
2022-11-02T00:00:00+0000
fractal video
Historical Mandelbrot zoom.

# The Ant With 22 Legs

Historical Mandelbrot zoom.

A zoom into a location in the Mandelbrot set,
coordinates originally published in the
[Amygdala newsletter issue 7](https://archive.org/details/amygdala-a-newsletter-of-fractals-m-the-mandelbrot-set-issue-07).
Iteration counts above 256 (the original dwell limit) are coloured red,
pixels are shaded by distance estimate.

Making this video took a few minutes wall-clock time on modern hardware
(AMD Ryzen 2700X CPU, AMD RX 580 GPU).

Exponential map keyframes rendered with Fraktaler-3 every 2x zoom factor at 12288x1360 with 1 sample per pixel,
reprojected to 1920x1080p60 video with 100 samples per pixel using Zoomasm-4 (prerelease).
Iteration limit was 10M total, 10k steps, using bilinear approximation for acceleration.

## Video

```=html
<video src="https://media.mathr.co.uk/mathr/2022-the-ant-with-22-legs/mathr%20-%202022-11-02%20-%20the%20ant%20with%2022%20legs%20-%20640x360p30.mp4" poster="https://media.mathr.co.uk/mathr/2022-the-ant-with-22-legs/mathr%20-%202022-11-02%20-%20the%20ant%20with%2022%20legs%20-%20640x360p30.jpg" controls="controls">
<a href="https://media.mathr.co.uk/mathr/2022-the-ant-with-22-legs/mathr%20-%202022-11-02%20-%20the%20ant%20with%2022%20legs%20-%20640x360p30.mp4">640x360p30 MP4</a> (7MB)
</video>
```

[1920x1080p60 MP4](https://media.mathr.co.uk/mathr/2022-the-ant-with-22-legs/mathr%20-%202022-11-02%20-%20the%20ant%20with%2022%20legs%20-%201920x1080p60.mkv) (71 MB), 1m00s, no sound

Parameters:
[F3 TOML](https://media.mathr.co.uk/mathr/2022-the-ant-with-22-legs/mathr%20-%202022-11-02%20-%20the%20ant%20with%2022%20legs.f3.toml) /
[ZA TOML](https://media.mathr.co.uk/mathr/2022-the-ant-with-22-legs/mathr%20-%202022-11-02%20-%20the%20ant%20with%2022%20legs.za.toml) /
[ZA GLSL](https://media.mathr.co.uk/mathr/2022-the-ant-with-22-legs/mathr%20-%202022-11-02%20-%20the%20ant%20with%2022%20legs.za.glsl)
