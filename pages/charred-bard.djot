Charred Bard
2020-09-07T00:00:00+0000
fractal music video
Hybrid fractal deep zoom techno music video.

# Charred Bard

Zoom into a hybrid fractal, in black on white with rainbow fringes.

59 iterations of Mandelbrot Set formula, then 1 iteration of Burning Ship formula, then repeat, until escape.

137bpm techno soundtrack is composed in Clive,
using tempo-synced feedback delays with 1/2-octave bandpass filter bank,
where each band's gain depends on the level of the bands below.

Rendered as an exponential map with KF,
using a custom zoom assembler to flatten it into video frames.

## Video

```=html
<video src="https://media.mathr.co.uk/mathr/2020-charred-bard/mathr%20-%202020-09-07%20-%20charred%20bard%20-%20640x360p30.mp4" poster="https://media.mathr.co.uk/mathr/2020-charred-bard/mathr%20-%202020-09-07%20-%20charred%20bard%20-%20640x360p30.jpg" controls="controls">
<a href="https://media.mathr.co.uk/mathr/2020-charred-bard/mathr%20-%202020-09-07%20-%20charred%20bard%20-%20640x360p30.mp4">640x360p30 MP4</a> (63MB)
</video>
```

[1920x1080p60 MP4](https://media.mathr.co.uk/mathr/2020-charred-bard/mathr%20-%202020-09-07%20-%20charred%20bard%20-%201920x1080p60.mp4) 1045MB, 6m06s, stereo sound
