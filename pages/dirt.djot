Dirt
2025-01-27T16:30:00+0000
dsp livecode
An unimpressive thingie for playing bits of samples with some level of accuracy.

# Dirt

An unimpressive thingie for playing bits of samples with some level of accuracy.

By Alex McLean and contributors since 2015, licensed under GPLv3.

Commonly used with live-coding environments like Tidal.

## Upstream

<https://github.com/tidalcycles/Dirt>

```sh
git clone --recursive -b 1.1-dev https://github.com/tidalcycles/Dirt.git
```

Omit `--recursive` to avoid downloading ~200MB of
audio samples of dubious provenance.

## My Fork

May contain experiments that haven't made their way to github yet.

```sh
git clone -b 1.1-dev https://code.mathr.co.uk/Dirt.git
```

## Audio Backends

I made the audio backend selectable at runtime with the `-o` flag,
with multiple audio backends able to be compiled in at the same time.
I also added a new audio backend using SDL2.

## Graphical User Interface

I made a small graphical user interface `dirt-gui`
that allows configuring settings and launching the Dirt engine.
It is implemented in SDL2 with Dear ImGui requiring OpenGL 2.

## Windows

I've been working on cross-compiling for Windows from Linux,
current status is
"it mostly works in Wine (but PortAudio is choppy), Microsoft Windows is untested".
No clean engine exit so to edit settings you have to restart the program.

You can download preliminary / beta unofficial Windows EXEs for testing here:

- [dirt-1.1-111-gc02ede3-windows.zip](dirt/dirt-1.1-111-gc02ede3-windows.zip) ([sig](dirt/dirt-1.1-111-gc02ede3-windows.zip.sig))

The Cygwin compilation instructions have probably bitrotted, help wanted.

## macOS

The macOS compilation instructions have probably bitrotted, help wanted.

## Android

I've been working on an Android version, current status is
"it mostly works, but no clean exit, and setting up samples is a pain".

You can download preliminary / beta unofficial Android APK for testing here:

- [dirt-1.1-111-gc02ede3.apk](dirt/dirt-1.1-111-gc02ede3.apk) ([sig](dirt/dirt-1.1-111-gc02ede3.apk.sig))

### Samples on Android

Dirt for Android doesn't come with any samples.
You need to install them separately.
Depending on Android version, permissions, etc,
you might have to put them internal storage,
and external SD card won't be recognized.

Samples are expected to be in
`Storage` / `Android` / `data` / `${DIRT_PACKAGE}` / `files` / `${SAMPLES}` / `${SAMPLE_NAME}` / `${SAMPLE_FILE}.wav`.

You can check you have the right place: starting Dirt creates
a file called `README-samples-go-here.html` in the relevant `files` folder.

To get the default Dirt samples follow these steps
using your usual Android file manager
(maybe you are lucky and you can replace Internal Storage with SD):

1. On your Android device, download `Dirt-Samples` as a zip archive:
   <https://github.com/tidalcycles/Dirt-Samples/archive/refs/heads/master.zip> (170MB).

2. Move the zip to `Internal Storage` / `Android` / `data` / `${DIRT_PACKAGE}` / `files`.

3. Unzip the zip (this will take up another 225MB).

4. You can now delete the zip archive to save space.

On Dirt startup, activate the `Samples Root Path` button
and choose the samples folder in the file dialog:
choose the parent directory, `Dirt-Samples-master`,
of the folders containing the WAV files.

To avoid having to choose on every Dirt startup, you can rename
the folder to the default, `...` / `files` / `samples`
using your usual Android file manager:

1. If your unzipper helpfully made an extra directory, like `master/Dirt-Samples-master`,
   move the `Dirt-Samples-master` directory to be inside the parent directory `files`.

2. Rename the `Dirt-Samples-master` directory to `samples`.

3. You should end up `...` / `${DIRT_PACKAGE}` / `files` / `samples`,
   with many folders inside, each containing WAV sample files.
