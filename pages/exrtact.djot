Exrtact
2023-09-12T14:20:00+0100
graphics
Various tools to manipulate EXR images.

# Exrtact

Various tools to manipulate EXR images.

## Get

### Source

Browse at [code.mathr.co.uk/exrtact](https://code.mathr.co.uk/exrtact).

```
sudo apt install git make g++ pkg-config libopenexr-dev libpng-dev
git clone https://code.mathr.co.uk/exrtact.git
cd exrtact
make
```

### Windows

(out of date)

- [exrtact-x86_64.7z](exrtact/exrtact-x86_64.7z) ([sig](exrtact/exrtact-x86_64.7z.sig))
- [exrtact-i686.7z](exrtact/exrtact-i686.7z) ([sig](exrtact/exrtact-i686.7z.sig))

## Usage

### Channel Extraction

```
exrtact in.exr out.exr chan
```

- `in.exr` is the input file, it should have a channel named `chan`
  which will be extracted
- `out.exr` is the output file which will be created, it will have
  a channel named `Y` which contains the extracted channel

### Preview Extraction

```
exrtact-preview in.exr out.png
```

- `in.exr` is the input file, it should have a preview image
  which will be extracted
- `out.png` is the output file which will be created, it will have
  RGBA8 channels containing the extracted preview image

### Image Tiling

```
exrtactile stem factor stratify out.exr [channel [channel [...]]]
```

- combines `stem-YYYY-XXXX.exr` into `out.exr`
  where each `XXXX`, `YYYY` is zero-padded
  4 decimal digits from `0` to `factor - 1`.
  `X` increases left to right.
  `Y` increases top to bottom.
- if `stratify` is `0`, does regular tiling
  (the images are placed side by side)
- if `stratify` is `1`, does "stratified" tiling
  (the pixels get interleaved)
- if channel list is specified, only those channels are used,
  otherwise all channels are used

### Image Reduction

```
exrsubsample factor in.exr out.exr [channel [channel [...]]]
```

- subsamples `in.exr` by `factor` into `out.exr`
- if channel list is specified, only those channels are used,
  otherwise all channels are used

### Histogram Quantiles

```
exrquantile nbins in.exr channel
```

- sorts the data of channel, groups it into bins with equal sizes,
  and outputs the quantities at bin boundaries
- output is tab separated values with header
- uint32, float32, float16 (half) data are supported

### Channel Statistics

```
exrstats in.exr [channel [channel [...]]]
```

- outputs minimum, maximum, mean, standard deviation of each channel
- output is tab separated values with header
- uint32, float32, float16 (half) data are supported
- if channel list is specified, only those channels are used,
  otherwise all channels are used

### Image Averaging

```
exrmean out.exr in1.exr [in2.exr [in3.exr [... ]]]
```

- averages R G B input images (arithmetic mean)
- other channels are ignored
- all images must have the same dimensions

### Plain Text Conversion

```
exrplain in.exr channel > out.txt
```

- prints channel data as decimal numbers

### Distance Estimate Equalisation

```
exrdeeq in.exr out.exr
```

- reads DEX and DEY channels
- outputs Y channel
- DE less than 0 becomes 0
- DE greater than 1 becomes 1
- otherwise Y is the index of DE in the sorted array, scaled to the range 0 to 1

## Bugs

- no license

### Channel Extraction Bugs

- only single part scanline flat images are supported
- only float32 channels are supported
  (OpenEXR may cast other types on read, potentially losing data
  or expanding space usage for no gain)

### Preview extraction Bugs

- only single part images are supported

### Image Tiling Bugs

- preview images are not processed
- only single part scanline flat images are supported
- only one factor for both dimensions

### Image Reduction Bugs

- preview images are not processed
- only single part scanline flat images are supported
- only one factor for both dimensions
- no filtering before decimation
- no control over which subpixel is selected (always top left)

### Histogram Quantiles Bugs

- float16 (half) data is output with misleadingly high precision

### Image Averaging Bugs

- only float16 R, G, B is supported

### Plain Text Conversion Bugs

- float16 (half) data is output with misleadingly high precision

### Distance Estimate Equalisation Bugs

- just sorts the data, instead of using histograms with bigger buckets
- thresholds are not configurable
