# web

<https://mathr.co.uk/web/> website engine and content.

## quick start

```
git clone -b mathr https://code.mathr.co.uk/djot.lua.git
git clone https://code.mathr.co.uk/web.git
cd web
./build.sh
```

See `html/web-engine.html` for more documentation
(`pages/web-engine.djot` before building).
