#!/bin/sh
now="$(date --iso=s)"
site="mathr"
url="https://mathr.co.uk/web"
author="Claude Heiland-Allen"
generator="https://code.mathr.co.uk/web"
djot="djot.lua"
mkdir -p html index
for md in pages/*.djot
do
  if [ "${md}" = 'pages/*.djot' ]
  then
    continue
  fi
  page="${md%.djot}"
  page="${page#pages/}"
  if [ "${page#_}" = "${page}" ]
  then
    html="html/${page}.html"
    [ "${html}" -nt "build.sh" -a "${html}" -nt "${md}" ] ||
    (
      echo "${html}" >&2
      read title
      read date
      read keywords
      read description
      read separator
      if [ "${separator}" ]
      then
        echo "missing separator in ${md}" >&2
        exit 1
      fi
      year="${date%-*}"
      for keyword in everything ${keywords}
      do
        ix="index/${keyword}.ix"
        touch "${ix}"
        grep -q -s "[^ ]* ${page} .*$" "${ix}" &&
        (
          grep -v "^[^ ]* ${page} .*$" > "${ix}.new" < "${ix}"
          mv -f "${ix}.new" "${ix}"
        )
        echo "${date} ${page} ${title}" >> "${ix}"
      done
      cat << EOF
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" >
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>${title} :: ${site}</title>
<meta property="og:title" content="${title} :: ${site}">
<meta name="description" content="${description}">
<meta property="og:description" content="${description}">
<meta name="keywords" content="${keywords}">
<link rel="canonical" href="${url}/${page}.html">
<meta property="og:url" content="${url}/${page}.html">
<meta name="generator" content="${generator}">
<meta name="author" content="${author}">
<link rel="stylesheet" type="text/css" href="style.css" title="default">
<link rel="alternate stylesheet" type="text/css" href="print.css" title="print">
<meta property="og:locale" content="en_GB">
<meta property="og:type" content="website">
<meta property="og:image" content="${url}/favicon.png">
<link rel="icon" href="favicon.ico">
<link rel="icon" href="favicon.png" type="image/png">
<meta name="twitter:card" content="summary">
<script src="/mathjax/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
</head>
<body id="top">
<header>
<nav><ol>
<li><a href=".">index</a></li>
EOF
      for keyword in ${keywords}
      do
        cat << EOF
<li><a href="${keyword}.html">${keyword}</a></li>
EOF
      done
      cat << EOF
</ol></nav>
</header>
<main>
EOF
      LUA_PATH="?.lua;../${djot}/?.lua;$LUA_PATH" lua5.4 "../${djot}/bin/main.lua" --filter toc
      cat << EOF
</main>
<footer><nav><ol>
<li><a href="#top">#</a></li>
<li><a href="https://mathr.co.uk">mathr.co.uk</a></li>
</ol></nav></footer>
</body>
</html>
EOF
    ) > "${html}" < "${md}"
  fi
done

# -------------------------------------------------------------------
for ix in index/*.ix
do
  if [ "${ix}" = 'index/*.ix' ]
  then
    continue
  fi
  page="${ix%.ix}"
  page="${page#index/}"
  html="html/${page}.rss"
  md="pages/_${page}.djot"
  [ "${html}" -nt "build.sh" -a "${html}" -nt "${ix}" -a "${html}" -nt "${md}" ] ||
  (
    echo "${html}" >&2
    (
      read title
      read date
      read keywords
      read description
      read separator
      if [ "${separator}" ]
      then
        echo "missing separator in ${md}" >&2
        exit 1
      fi
      cat << EOF
<?xml version="1.0" encoding="UTF-8"?>
<rss xmlns:atom="http://www.w3.org/2005/Atom" version="2.0">
<channel>
<atom:link href="${url}/${page}.rss" rel="self" type="application/rss+xml" />
<title>${title} :: ${site}</title>
<link>${url}/${page}.html</link>
<description>${description}</description>
EOF
    ) < "${md}"
    sort -r |
    while read date href title
    do
      cat << EOF
<item>
<title>${title}</title>
<link>${url}/${href}.html</link>
<guid>${url}/${href}.html?ts=$(date -d "${date}" +%s)</guid>
<pubDate>$(date -d "${date}" -R -u)</pubDate>
</item>
EOF
    done
    cat << EOF
</channel>
</rss>
EOF
  ) > "${html}" < "${ix}"
done

# -------------------------------------------------------------------
for ix in index/*.ix
do
  if [ "${ix}" = 'index/*.ix' ]
  then
    continue
  fi
  page="${ix%.ix}"
  page="${page#index/}"
  html="html/${page}.html"
  md="pages/_${page}.djot"
  [ "${html}" -nt "build.sh" -a "${html}" -nt "${ix}" -a "${html}" -nt "${md}" ] ||
  (
    echo "${html}" >&2
    (
      read title
      read date
      read keywords
      read description
      read separator
      if [ "${separator}" ]
      then
        echo "missing separator in ${md}" >&2
        exit 1
      fi
      cat << EOF
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>${title} :: ${site}</title>
<meta property="og:title" content="${title} :: ${site}">
<meta name="description" content="${description}">
<meta property="og:description" content="${description}">
<meta name="keywords" content="${keywords}">
<link rel="canonical" href="${url}/${page}.html">
<link rel="alternate" type="application/rss+xml" href="${page}.rss" title="${title} :: ${site}">
<meta property="og:url" content="${url}/${page}.html">
<meta name="generator" content="${generator}">
<meta name="author" content="${author}">
<link rel="stylesheet" type="text/css" href="style.css" title="default">
<link rel="alternate stylesheet" type="text/css" href="print.css" title="print">
<meta property="og:locale" content="en_GB">
<meta property="og:type" content="website">
<meta property="og:image" content="${url}/favicon.png">
<link rel="icon" href="favicon.ico">
<link rel="icon" href="favicon.png" type="image/png">
<meta name="twitter:card" content="summary">
<script src="/mathjax/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
</head>
<body id="top">
<header>
<nav>
<a class="feed-icon" href="${page}.rss" title="RSS feed :: ${title} :: ${site}"><img alt="RSS" src="feed-icon.svg"></a>
<ol>
<li><a href=".">index</a></li>
</ol>
</nav>
</header>
<main>
EOF
      LUA_PATH="?.lua;../${djot}/?.lua;$LUA_PATH" lua5.4 "../${djot}/bin/main.lua" --filter toc
    ) < "${md}"
    cat << EOF
<section id="${page}-index" class="index">
<ol>
EOF
    sort -r |
    while read date href title
    do
      cat << EOF
<li><span class="date">${date%T*}</span> <a href="${href}.html">${title}</a></li>
EOF
    done
    cat << EOF
</ol>
</section>
</main>
<footer><nav><ol>
<li><a href="#top">#</a></li>
<li><a href="https://mathr.co.uk">mathr.co.uk</a></li>
</ol></nav></footer>
</body>
</html>
EOF
  ) > "${html}" < "${ix}"
done

# -------------------------------------------------------------------
page="index"
html="html/${page}.html"
md="pages/_${page}.djot"
# always regenerate
(
  echo "${html}" >&2
  (
    read title
    read date
    read keywords
    read description
    read separator
    if [ "${separator}" ]
    then
      echo "missing separator in ${md}" >&2
      exit 1
    fi
    cat << EOF
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>${title} :: ${site}</title>
<meta property="og:title" content="${title} :: ${site}">
<meta name="description" content="${description}">
<meta property="og:description" content="${description}">
<meta name="keywords" content="${keywords}">
<link rel="canonical" href="${url}/${page}.html">
<meta property="og:url" content="${url}/${page}.html">
<meta name="generator" content="${generator}">
<meta name="author" content="${author}">
<link rel="stylesheet" type="text/css" href="style.css" title="default">
<link rel="alternate stylesheet" type="text/css" href="print.css" title="print">
<meta property="og:locale" content="en_GB">
<meta property="og:type" content="website">
<meta property="og:image" content="${url}/favicon.png">
<link rel="icon" href="favicon.ico">
<link rel="icon" href="favicon.png" type="image/png">
<meta name="twitter:card" content="summary">
<script src="/mathjax/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>
</head>
<body id="top">
<header>
<nav>
<a class="feed-icon" href="everything.rss" title="RSS feed :: ${title} :: ${site}"><img alt="RSS" src="feed-icon.svg"></a>
<ol>
<li><a href=".">index</a></li>
</ol>
</nav>
</header>
<main>
EOF
    LUA_PATH="?.lua;../${djot}/?.lua;$LUA_PATH" lua5.4 "../${djot}/bin/main.lua" --filter toc
  ) < "${md}"
  cat << EOF
<section id="Topics" class="index">
<h2><a href="#Topics">#</a> Topics</h2>
<ol>
EOF
  for ix in index/*.ix
  do
    if [ "${ix}" = 'index/*.ix' ]
    then
      continue
    fi
    index="${ix%.ix}"
    index="${index#index/}"
    if [ "${index}" != "everything" ]
    then
      cat << EOF
<li><a href="${index}.html">${index}</a></li>
EOF
    fi
  done
  cat << EOF
<li><a href="everything.html">*</a></li>
</ol>
</section>
<section id="Recent" class="index">
<h2><a href="#Recent">#</a> Recent</h2>
<ol>
EOF
  sort -r index/everything.ix |
  head -n 10 |
  while read date href title
  do
    cat << EOF
<li><span class="date">${date%T*}</span> <a href="${href}.html">${title}</a></li>
EOF
  done
  cat << EOF
</ol>
</section>
</main>
<footer><nav><ol>
<li><a href="#top">#</a></li>
<li><a href="https://mathr.co.uk">mathr.co.uk</a></li>
</ol></nav></footer>
</body>
</html>
EOF
) > "${html}"
