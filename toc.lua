local toc_depth = 0
local toc_write = 0
local counters = { 0, 0, 0, 0, 0, 0 }
return {
  section = {
    enter = function(e)
      if toc_depth == 0 then
        io.stdout:write("<nav id='toc'><a href='#toc'>#</a>\n")
      end
      toc_depth = toc_depth + 1
      if toc_depth > 1 then
        io.stdout:write("<li>")
      end
    end,
    exit = function(e)
      if toc_depth > 0 then
        io.stdout:write("</ol>")
      end
      if toc_depth > 1 then
        io.stdout:write("</li>")
      end
      toc_depth = toc_depth - 1
      if toc_depth == 0 then
        io.stdout:write("</nav>\n")
      end
    end
  },
  heading = {
    enter = function(e)
      counters[e.level] = counters[e.level] + 1
      for l = e.level + 1, 6 do
        counters[l] = 0
      end
      io.stdout:write("&#x2022; ")
      e.counters = ""
      for l = 2, e.level do
        e.counters = e.counters .. tostring(counters[l])
        if l < e.level then
          e.counters = e.counters .. "."
        end
      end
      if e.level > 1 then
        e.counters = e.counters .. " "
        io.stdout:write(e.counters)
      end
      io.stdout:write("<a href='#" .. e.section.attr.id .. "'>")
      toc_write = toc_write + 1
    end,
    exit = function(e)
      toc_write = toc_write - 1
      io.stdout:write("</a><ol>\n")
    end,
  },
  str = function(e)
    if toc_write > 0 then
      io.stdout:write(e.text)
    end
  end
}
