#version 330 core
#info Parameter plane of z -> z^2 + c z^5 by Claude Heiland-Allen 2023
/*
green: critical point proven to escape to infinity (center) or remain bounded (outside)
red: no decision reached after 1000 iterations
light: critical point escaped to infinity (filled-in Julia set has infinitely-many components)
dark: critical point escaped to 0 (filled-in Julia set has a single component)
*/

#include "TwoD.frag"

const float pi = 3.141592653;

vec2 cMul(vec2 a, vec2 b)
{
  return vec2(a.x * b.x - a.y * b.y, a.x * b.y + a.y * b.x);
}

vec2 cConj(vec2 a)
{
  return vec2(a.x, -a.y);
}

vec2 cDiv(vec2 a, vec2 b)
{
  return cMul(a, cConj(b) / dot(b, b));
}

vec2 cExp(vec2 a)
{
  return exp(a.x) * vec2(cos(a.y), sin(a.y));
}

vec2 cLog(vec2 a)
{
  return vec2(log(length(a)), atan(a.y, a.x));
}

vec2 cCbrt(vec2 a, int n)
{
  return cExp((cLog(a) + vec2(0, float(n) * 2.0 * pi)) / 3.0);
}

vec3 color(vec2 c, vec2 dx, vec2 dy) {
  bvec3 exscaped = bvec3(false);
  bvec3 inscaped = bvec3(false);
  ivec3 n = ivec3(1000);
  float r = 5.0 / 7.0;
  float R = 19.0 / (15.0 * pow(length(c), 1.0 / 3.0));
  for (int j = 0; j < 3; ++j)
  {
    vec2 z = cCbrt(cDiv(vec2(-2.0/5.0, 0.0), c), j);
    for (int i = 0; i < 1000; ++i)
    {
      n[j] = i;
      exscaped[j] = exscaped[j] || !(length(z) < R);
      if (exscaped[j]) break;
      inscaped[j] = inscaped[j] || !(length(z) > r);
      if (inscaped[j]) break;
      vec2 z2 = cMul(z, z);
      vec2 z4 = cMul(z2, z2);
      vec2 cz4 = cMul(c, z4);
      z = cMul(z, z + cz4);
    }
  }
  vec3 grid = vec3(1,1,1);
  vec2 g = c;
  for (int i = 0; i < 10; ++i)
  {
    g += 0.5;
    g -= floor(g);
    g -= 0.5;
    if (abs(g.y) < 0.01) grid *= vec3(0.9,0.9,1);
    if (abs(g.x) < 0.01) grid *= vec3(0.9,0.9,1);
    g *= 10.0;
  }
  return pow(grid * mix(mix(mix(0.5 * (vec3(exscaped) - vec3(inscaped) + vec3(1.0)), vec3(0,1,0), float((length(c) > (2.0 * 7.0 * 7.0 * 7.0) / (5.0 * 5.0 * 5.0 * 5.0) || 0 < length(c) && length(c) < 0.01))), vec3(0.5), vec3(n & 1) * 0.5), vec3(1,0,0), vec3(n == ivec3(1000-1))), vec3(2.0));
}

#preset Whole
Center = 0,0
Zoom = 0.75
#endpreset

#preset Zoomed
Center = 0,0
Zoom = 4
#endpreset

#preset SpecialC
Center = 0.2294487977025897,0
Zoom = 30
#endpreset
