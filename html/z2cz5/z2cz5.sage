f(c, z) = z^2 + c * z^5
for cp in solve(diff(f(c, z), z) == 0, z):
  try:
    g(c) = (f(c, f(c, z)) - f(c, z)).substitute(cp).expand()
    h(x) = (g(x^3) * x^7).canonicalize_radical()
    P = sum([ QQbar(H[0]) * polygen(QQbar)^H[1] for H in h(x).coefficients() ])
    c0 = QQbar.polynomial_root(P, CIF(RIF(0.6, 0.7), RIF(-0.1, 0.1)))^3
    z0 = QQbar(- (2 / (5 * c0))^(1/3))
    z1 = QQbar((- 2 / (5 * c0))^(1/3))
    print(all([f(c0, z0) == f(c0, f(c0, z0)), f(c0, z1) == f(c0, f(c0, f(c0, z1)))]))
    print(h(x))
    print(P)
    print(CIF(c0))
  except:
    pass
