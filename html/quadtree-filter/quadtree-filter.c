// gcc -std=c99 -Wall -Wextra -pedantic -O3 -DNDEBUG -o quadtree-filter quadtree-filter.c

// references:
// - <https://gitlab.com/kurbitur/quadtree-image-filter>
// - <https://github.com/fogleman/Quads>
// - <https://en.wikipedia.org/wiki/Summed-area_table>

#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

// interleaved rgbrgbrgb to planar rrrgggbbb
extern
void
deinterleave
  ( int width, int height
  , const uint8_t * restrict rgb
  , uint8_t * restrict planes
  )
{
  for (int y = 0; y < height; ++y)
  {
    for (int x = 0; x < width; ++x)
    {
      for (int channel = 0; channel < 3; ++channel)
      {
        planes[(channel * height + y) * width + x]
          = rgb[(y * width + x) * 3 + channel];
      }
    }
  }
}

// planar rrrgggbbb to interleaved rgbrgbrgb
extern
void
interleave
  ( int width, int height
  , uint8_t * restrict rgb
  , const uint8_t * restrict planes
  )
{
  for (int y = 0; y < height; ++y)
  {
    for (int x = 0; x < width; ++x)
    {
      for (int channel = 0; channel < 3; ++channel)
      {
        rgb[(y * width + x) * 3 + channel]
          = planes[(channel * height + y) * width + x];
      }
    }
  }
}

// calculate summed area tables
extern
void
preprocess
  ( int width, int height
  , const uint8_t * restrict pixel
  , uint32_t * restrict I1
  , uint64_t * restrict I2
  )
{
  {
    // y = 0
    uint32_t sum1 = 0;
    uint32_t sum2 = 0;
    for (int x = 0; x < width; ++x)
    {
      int k = x;
      uint16_t i = pixel[k];
      I1[k] = (sum1 += i    );
      I2[k] = (sum2 += i * i);
    }
  }
  for (int y = 1; y < height; ++y)
  {
    uint32_t sum1 = 0;
    uint32_t sum2 = 0;
    for (int x = 0; x < width; ++x)
    {
      int k = y * width + x;
      int l = k - width;
      uint16_t i = pixel[k];
      I1[k] = (sum1 += i    ) + I1[l];
      I2[k] = (sum2 += i * i) + I2[l];
    }
  }
}

// fill a rectangle
extern
void
fill
  ( uint8_t mean
  , int x0, int y0, int x1, int y1
  , int width, uint8_t * restrict pixel
  )
{
  for (int y = y0; y < y1; ++y)
    for (int x = x0; x < x1; ++x)
      pixel[y * width + x] = mean;
}

// calculate quadtree filter effect
extern
void
process
  ( uint64_t threshold             // variance threshold
  , int x0, int y0, int x1, int y1 // x0 <= x < x1, y0 <= y < y1
  , uint32_t s1tl, uint32_t s1tr   // summed area values at corners
  , uint32_t s1bl, uint32_t s1br
  , uint64_t s2tl, uint64_t s2tr
  , uint64_t s2bl, uint64_t s2br
  , int width                      // width for arrays
  , uint8_t * restrict pixel       // [3 * width * height]
  , const uint32_t * restrict I1   // [width * height]
  , const uint64_t * restrict I2   // [width * height]
  )
{
  // check invariants
  // omitted with compiling with -DNDEBUG
  assert(0 <= x0);
  assert(x0 < x1);
  assert(x1 <= width);
  assert(0 <= y0);
  assert(y0 <= y1);
  assert(s1tl <= s1tr);
  assert(s1bl <= s1br);
  assert(s1tl <= s1bl);
  assert(s1tr <= s1br);
  assert(s2tl <= s2tr);
  assert(s2bl <= s2br);
  assert(s2tl <= s2bl);
  assert(s2tr <= s2br);

  // compute sums for variance calculation
  uint32_t S0 = (y1 - y0) * (x1 - x0);
  uint32_t S1 = s1br + s1tl - s1tr - s1bl;
  uint64_t S2 = s2br + s2tl - s2tr - s2bl;

  // don't divide variance by S0*S0, instead multiply the threshold
  // the process is guaranteed to terminate
  // because the variance of a single cell is 0
  uint64_t variance = S0 * S2 - (uint64_t) S1 * S1;
  // alternatively:
  //   uint64_t variance = S2 - (uint64_t) S1 * S1 / S0;
  // would allow bigger images (less overflow risk), but be inexact
  if (variance <= S0 * S0 * threshold)
  {

    // variance is small
    // fill with mean value
    // division truncates, add a bit for bidirectional rounding
    uint8_t mean = (S1 + (S0 >> 1)) / S0;
    fill(mean, x0, y0, x1, y1, width, pixel);

  }
  else
  {

    // variance is large
    // recurse on 2 halves,
    // splitting longest dimension
    // prefering landscape aspect if square
    // (to prefer portrait change >= to >)
    if (y1 - y0 >= x1 - x0)
    {
      // vertical split
      int y = (y0 + y1) >> 1;
      assert(y > 0);
      int k = (y - 1) * width;
      int k0 = k + (x0 - 1);
      int k1 = k + (x1 - 1);
      uint32_t s1ml = x0 == 0 ? 0 : I1[k0];
      uint32_t s1mr =               I1[k1];
      uint64_t s2ml = x0 == 0 ? 0 : I2[k0];
      uint64_t s2mr =               I2[k1];
      process
        ( threshold
        , x0, y0, x1, y
        , s1tl, s1tr, s1ml, s1mr
        , s2tl, s2tr, s2ml, s2mr
        , width, pixel, I1, I2
        );
      process
        ( threshold
        , x0, y, x1, y1
        , s1ml, s1mr, s1bl, s1br
        , s2ml, s2mr, s2bl, s2br
        , width, pixel, I1, I2
        );
    }

    else
    {
      // horizontal split
      int x = (x0 + x1) >> 1;
      assert(x > 0);
      int k0 = (x - 1) + (y0 - 1) * width;
      int k1 = (x - 1) + (y1 - 1) * width;
      uint32_t s1tm = y0 == 0 ? 0 : I1[k0];
      uint32_t s1bm =               I1[k1];
      uint64_t s2tm = y0 == 0 ? 0 : I2[k0];
      uint64_t s2bm =               I2[k1];
      process
        ( threshold
        , x0, y0, x, y1
        , s1tl, s1tm, s1bl, s1bm
        , s2tl, s2tm, s2bl, s2bm
        , width, pixel, I1, I2
        );
      process
        ( threshold
        , x, y0, x1, y1
        , s1tm, s1tr, s1bm, s1br
        , s2tm, s2tr, s2bm, s2br
        , width, pixel, I1, I2
        );
    }

  }
}

extern
int
main(int argc, char **argv)
{

  // parse arguments
  if (argc < 2)
  {
    fprintf(stderr, "usage: %s threshold < input.ppm > output.ppm\n", argv[0]);
    return 1;
  }
  int threshold = atoi(argv[1]);
  threshold *= threshold; // turn stddev into variance

  // buffers
  // don't reallocate each frame unless necessary
  int allocated_size = 0;
  uint8_t * restrict rgb = 0;    // interleaved rgbrgbrgb
  uint8_t * restrict planes = 0; // planar rrrgggbbb
  uint32_t * restrict I1 = 0;    // summed-area table of intensity
  uint64_t * restrict I2 = 0;    // summed-area table of intensity^2

  // work as a filter on a PPM image stream
  int result, width, height;
  while (2 == (result = scanf("P6\n%d %d 255", &width, &height)))
  {
    if ('\n' != getchar())
    {
      fprintf(stderr, "error: expected newline after image header\n");
      break;
    }
    if (width * height > 4096 * 4096)
    {
      // use of uint32_t for intensity table
      // and uint64_t for variance calculations are
      // only safe up to about 4096x4096 pixels
      // bigger than that arithmetic overflow becomes possible
      fprintf(stderr, "error: image size too large\n");
      break;
    }

    // allocate memory
    // if the buffer are already big enough, reuse the memory
    int size_to_allocate = width * height;
    if (size_to_allocate > allocated_size)
    {

      // deallocate any pre-existing buffers
      if (rgb)
      {
        free(rgb);
        rgb = 0;
      }
      if (planes)
      {
        free(planes);
        planes = 0;
      }
      if (I1)
      {
        free(I1);
        I1 = 0;
      }
      if (I2)
      {
        free(I2);
        I2 = 0;
      }

      // allocate new buffers
      rgb = malloc(width * height * 3);
      if (! rgb)
      {
        fprintf(stderr, "error: memory allocation failed\n");
        break;
      }
      planes = malloc(width * height * 3);
      if (! planes)
      {
        fprintf(stderr, "error: memory allocation failed\n");
        break;
      }
      I1 = calloc(1, width * height * sizeof(*I1));
      if (! I1)
      {
        fprintf(stderr, "error: memory allocation failed\n");
        break;
      }
      I2 = calloc(1, width * height * sizeof(*I2));
      if (! I2)
      {
        fprintf(stderr, "error: memory allocation failed\n");
        break;
      }

      // keep track of available space
      allocated_size = size_to_allocate;
    }

    // read input image pixels
    if (1 != fread(rgb, width * height * 3, 1, stdin))
    {
      fprintf(stderr, "error: read failed\n");
      break;
    }

    // process each channel independently
    deinterleave(width, height, rgb, planes);
    for (int channel = 0; channel < 3; ++channel)
    {
      uint8_t * restrict pixels = planes + width * height * channel;
      preprocess
        ( width, height
        , pixels
        , I1
        , I2
        );
      process
        ( threshold
        , 0, 0, width, height
        , 0, 0, 0, I1[width * height - 1]
        , 0, 0, 0, I2[width * height - 1]
        , width
        , pixels
        , I1
        , I2
        );
    }
    interleave(width, height, rgb, planes);

    // output image
    if (0 > printf("P6\n%d %d\n255\n", width, height))
    {
      fprintf(stderr, "error: write failed\n");
      break;
    }
    if (1 != fwrite(rgb, width * height * 3, 1, stdout))
    {
      fprintf(stderr, "error: write failed\n");
      break;
    }

  }

  // deallocate memory
  if (rgb)
  {
    free(rgb);
  }
  if (planes)
  {
    free(planes);
  }
  if (I1)
  {
    free(I1);
  }
  if (I2)
  {
    free(I2);
  }

  // all is ok if we reached the end of the input image stream
  return result != EOF;
}
