#include <stdio.h>

void print(const char *a)
{
  if (*a)
  {
    const char *b = a;
    while (*b)
    {
      ++b;
    }
    while (b != a)
    {
      putchar(*(--b));
    }
  }
  else
  {
    putchar('0');
  }
}

void set(char *out, const char *a)
{
  while (*a)
  {
    *out++ = *a++;
  }
  *out = 0;
}

void add(char *out, const char *a, const char *b)
{
  char carry = 0;
  while (*a && *b)
  {
    char sum = (*a++ - '0') + (*b++ - '0') + carry;
    carry = sum / 10;
    *out++ = (sum % 10) + '0';
  }
  while (*a)
  {
    char sum = (*a++ - '0') + carry;
    carry = sum / 10;
    *out++ = (sum % 10) + '0';
  }
  while (*b)
  {
    char sum = (*b++ - '0') + carry;
    carry = sum / 10;
    *out++ = (sum % 10) + '0';
  }
  if (carry)
  {
    *out++ = carry + '0';
  }
  *out = 0;
}

void mul1(char *out, const char *a, char b)
{
  out[0] = 0;
  while (b--)
  {
    add(out, out, a);
  }
}

void mul(char *out, const char *a, const char *b, char *tmp)
{
  out[0] = 0;
  while (*b)
  {
    mul1(tmp, a, *b++ - '0');
    add(out, out, tmp);
    ++out;
  }
}

void pow(char *out, const char *a, char b, char *tmp1, char *tmp2)
{
  out[0] = '1';
  out[1] = 0;
  while (b--)
  {
    set(tmp1, out);
    mul(out, tmp1, a, tmp2);
  }
}

int main()
{
#define DIGITS 255
  char result[DIGITS+1], tmp1[DIGITS+1], tmp2[DIGITS+1];
  const char base[] = "5732"; // reversed digits of 2375
  char power = 15;
  pow(result, base, power, tmp1, tmp2);
  print(result);
  putchar('\n');
  return 0;
}
